# **Photostock**
#### Интерфейс Photostock на основе APi Unsplash
# Getting Started
Эти инструкции позволят вам запустить копию проекта на вашем локальном компьютере в целях разработки и тестирования, а также является отчетом о проделанной работе.

# Development version
`git clone git@gitlab.com:Abibullaeva/photostock.git`

`npm i`

`npm run serve`

# Prodaction version 
Heroku : https://photostock-deployment-demo.herokuapp.com/favorite

# Процесс разработки
1. #### Изучение макета в [figma.com](https://www.figma.com/file/VHHUfQm7sQsziibED8EAa5/Rocket-frontend-test)

    Определение статичных блоков таких как Header и повторяющихся элементов, а также изучение мобильной версии для начала верстки.
2. #### Верстка.
    Создала проект через команду `vue create photostock`.

    Установила Axios через команду `npm i axios`.

    Установила BootstrapVue через команду `npm i bootstrap-vue`.

    Разбила проект на компоненты для отрисовки полученных данных с [API Unsplash](https://unsplash.com/developers).

    Определила пути в Router для таких страниц как: Главная страница, Избранное, История поиска, Фотография.

    В каждом компоненте создала блок со стилями.

    После приступила к верстке проекта.
3. #### Функционал.

    **Главная страница**

    Отрисовка рандомных фотографии. 
    Для этого в mounted я отправляю запрос, далее отрисовываю данные в компоненте Gallery.vue


    Создала SearchList в котором обозначила тэги API Unsplash, далее добавила событие клика на тэги, 
    отправляя  тэг выбранного пользователем. Код функционала отвечающего за поиск по тэгам и осуществляющего 
    отрисовку картин по этому тэгу:

```
    searchTabs(t) {
        let userId = 'DgyXHABTFsWExCj6qdkF_7z6lGzTyyBJnl7u6wsLilo'
        let url = 'https://api.unsplash.com/search/photos?query=' + t + '&client_id=' + userId
        Axios
          .get(url)
          .then(response => {
            this.gallery = response.data.results
          })
          .catch(error => console.log('error'));
    }, 
``` 

    В данной функции я сохраняю тэги на которые нажимает пользователь в LocalStorage для истории поиска:


```

    let photo_tags = t
        let tagsHistory = JSON.parse(localStorage.getItem('tagsHistory')) == null ? [] : JSON.parse(localStorage
          .getItem('tagsHistory'))
        if (tagsHistory.find(tag => tag == photo_tags)) {
          tagsHistory.forEach((item, index) => {
            if (item == photo_tags) {
              return false
            }
          })

        } else {
          JSON.stringify(tagsHistory.push(t))
        }
        localStorage.setItem('tagsHistory', JSON.stringify(tagsHistory));
```

    История поиска

    Полученные данные с LocalStorage я прошлась по циклу, записав каждый тэг в объект в котором помимо этого было 
    поле id со значением index. Эти объекты я добавила в пустой массив для отрисовки картин по истории поиска. 


    Избранное

    При каждом клике на иконку сердечка, я сохраняю id фотографии в LocalStorage и аналогично функционалу Истории поиска 
    прохожусь по циклу и отрисовываю избранные фотографии.


    Страница фотографии

    При клике на любую фотографию передаю id фотографии через params. На странице фотографии отправляю запрос используя этот id.
    С запроса получаю тэги которые использую для отрисовки похожих фотографии.

# Подведение итогов

    В ходе разработки интерфейса Photostock были реализованы задачи:
    

 1.   Главная страница со списком.
 2.   Поиск.
 3.   История поиска.
 4.   Страница фотографии.
 5.   Добавление в избранное.


